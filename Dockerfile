FROM node:12.7-alpine as node
RUN apk update && apk upgrade && apk add bash curl
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
RUN npm install -g @angular/cli@8.1.1
COPY . .
RUN npm run init
RUN ng build

# Stage 2
FROM nginx:1.13.12-alpine
COPY --from=node /usr/src/app/dist/fire-editor /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80 443