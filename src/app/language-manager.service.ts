import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

const lexon = "lex";
const sophia = "aes";

@Injectable({
  providedIn: 'root'
})
export class LanguageManagerService {
  // contractLanguage can be used for detecting 
  // contract language changes by subscribing to it.
  contractLanguage = new Subject<string>();

  constructor() {}

  // isLexon detects whether the code is a Lexon contract.
  private isLexon(code: string): boolean {
    return code.search(/LEX\s(.*)./i) !== -1;
  }

  // isSophia detects whether the code is a Sophia contect.
  private isSophia(code: string): boolean {
    return code.search(/contract\s([a-zA-Z0-9]*)\s?=/i) !== -1;
  }

  // Detect inspects the provided code and returns the language code.
  // Supports detection of:
  // aes: Sophia
  // lex: Lexon
  // To prevent breaking components using this service it will return
  // Sophia as the default
  detect(code: string): string {
    if(this.isLexon(code)) return lexon;

    // If the language is NOT Sophia in this stage, the language couldn't be detected.
    // That will be logged, but no error will be thrown.
    else if(!this.isSophia(code)) console.error('Contract has an unknown or undetectable language.')
    return sophia;
  }

  // DetectAndBroadcast inspects the provided code and returns + broadcasts
  // the language code. Supports detection of:
  // aes: Sophia
  // lex: Lexon
  detectAndBroadcast(code: string): string {
    const lang = this.detect(code);
    console.log('detected contract language: ' + lang);
    this.contractLanguage.next(lang);
    return lang;
  }
}
