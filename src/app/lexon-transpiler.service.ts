import { Injectable } from '@angular/core';
import { LexonWasm } from 'lexon-wasm';

@Injectable({
  providedIn: 'root'
})
export class LexonTranspilerService {
  private wasm: LexonWasm = new LexonWasm();

  toSophia(code: string): string {
    const r = this.wasm.compile(code);
    if (r != 'ok') {
      console.log(r);
      // const p = r.match(/-->\s?(\d):(\d)/i)
      const p = r.match(/-->\s?(\d*):(\d*)(?:(?:.|\n)*)=\s(.*)/i);
      // console.log(p);
      const line = p.length === 4 ? p[1] : 0;
      const col = p.length === 4 ? p[2] : 0; 
      // const e = r.match(/\=\s(.*)\s/im);
      // console.log(e);
      throw {'error': [{'type': 'lexon-transpiler', 'pos': {'col': col, 'line': line}, 'message': p[3]}]};
    }
    return this.wasm.sophia();
  }

  constructor() { }
}
