image:
	docker build -t sonemas/fire-editor:0.0.0 -t sonemas/fire-editor:latest .

push:
	docker push sonemas/fire-editor:0.0.0
	docker push sonemas/fire-editor:latest

deploy:
	ng build --prod --base-href https://5.lexon.tech/
	scp -P 2244 -r dist/fire-editor/* artje@5.lexon.tech:/var/www/fire-editor