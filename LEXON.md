# Fire-editor with Lexon integration

## Deploying

Deployment is optimized for Docker. The container will deploy with TLS enabled and redirects HTTP (port 80) traffic to TLS.

Getting TLS certificates is automated via letsencrypt.

### Environment variables

The following environment variables need to be set:
- DOMAIN : containing the FQD, i.e. `5.lexon.tech`
- EMAIL : containing the email if the TLS certificate

